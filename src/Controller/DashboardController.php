<?php

namespace App\Controller;

use App\Entity\ProductVariation;
use App\Repository\CategoryRepository;
use App\Repository\ProductRepository;
use App\Repository\ProductVariationRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class DashboardController extends AbstractController
{
    #[Route('/admin/dashboard', name: 'app_admin_dashboard', methods: ['GET'])]
    public function index(Request $request, ProductRepository $productRepository,CategoryRepository $categoryRepository,ProductVariationRepository $productVariationRepository): Response
    {
        return $this->render('dashboard/index.html.twig', [
            'controller_name' => 'DashboardController',
            'products' => $productRepository->findBy(
                array(),
                array('id' => 'DESC'),
                5,
                0
            ),
            'productVariation' => $productVariationRepository->findBy(
                array(),
                array('id' => 'DESC'),
                5,
                0
            ),

            'category' => $categoryRepository->findBy(
                array('parentid'=>null),
                array('title' => 'DESC'),
                5,
                0
            ),
            

        ]);

        

    }
}
