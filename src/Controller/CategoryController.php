<?php

namespace App\Controller;

use DateTimeImmutable;
use App\Entity\Category;
use App\Service\Uploader;
use App\Form\CategoryType;
use App\Repository\CategoryRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

#[Route('/admin/category')]
class CategoryController extends AbstractController
{
    #[Route('/', name: 'app_category_index', methods: ['GET'])]
    public function index(CategoryRepository $categoryRepository): Response
    {
        return $this->render('category/index.html.twig', [
            'categories' => $categoryRepository->findAll(),
        ]);
    }

    #[Route('/{id}/edit', name: 'app_category_edit', methods: ['GET', 'POST'])]
    #[Route('/new', name: 'app_category_new', methods: ['GET', 'POST'])]
    public function new(Category $category=null,Request $request, CategoryRepository $categoryRepository, EntityManagerInterface $entityManager, Uploader $fileUploader,SluggerInterface $sluggerInterface): Response
    {
        if($category == null)
            $category = new Category();

        $form = $this->createForm(CategoryType::class, $category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $pictureFile = $form->get('picture')->getData();
            
            if ($pictureFile) {
                $pictureFileName = $fileUploader->upload($pictureFile,'category');
                $category->setPicture($pictureFileName);
            }
            if(empty($category->getParentid())){
                $category -> setSlug($sluggerInterface->slug($category->getTitle())->lower());
            }
            else{
                $category -> setSlug($sluggerInterface->slug($category->getParentid()->getTitle(). '>' . $category->getTitle())->lower());
            }
           
          
            $category->setCreatedAt(new DateTimeImmutable('now'));

            $entityManager->persist($category);
            $entityManager->flush();

           // $categoryRepository->save($category, true);
            

            return $this->redirectToRoute('app_category_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('category/new.html.twig', [
            'category' => $category,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_category_show', methods: ['GET'])]
    public function show(Category $category): Response
    {
        return $this->render('category/show.html.twig', [
            'category' => $category,
        ]);
    }


    #[Route('/{id}', name: 'app_category_delete', methods: ['POST'])]
    public function delete(Request $request, Category $category, CategoryRepository $categoryRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$category->getId(), $request->request->get('_token'))) {
            $categoryRepository->remove($category, true);
        }

        return $this->redirectToRoute('app_category_index', [], Response::HTTP_SEE_OTHER);
    }
}
