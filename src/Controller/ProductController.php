<?php

namespace App\Controller;

use DateTimeImmutable;
use App\Entity\Picture;
use App\Entity\Product;
use App\Form\PictureType;
use App\Form\ProductType;
use App\Service\Uploader;
use App\Entity\ProductVariation;
use App\Form\ProductVariationType;
use App\Entity\Productvariationsize;
use App\Repository\PictureRepository;
use App\Repository\ProductRepository;
use App\Form\ProductVariationsizeType;
use Symfony\Component\HttpFoundation\Request;
use App\Repository\ProductVariationRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\ProductvariationsizeRepository;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

#[Route('/admin/product')]
class ProductController extends AbstractController
{

    //productlist
    #[Route('/', name: 'app_product_index', methods: ['GET'])]
    public function index(ProductRepository $productRepository): Response
    {
        return $this->render('product/index.html.twig', [
            'products' => $productRepository->findAll(),
        ]);
    }

    //productvariationlist
    #[Route('/variations/{id}', name: 'app_product_variation_list', methods: ['GET'])]
    public function productVariations(Product $product): Response
    {

        //dd($product->getProductVariations());
        //dd($product);
        return $this->render('product/listProductVariation.html.twig', [
            'product' => $product
            
        ]);
    }


    //listpicturevariation
    #[Route('/pictures/variation/{id}', name: 'app_admin_product_pictures_variation', methods: ['GET'])]
    public function listPicturesVariation(ProductVariation $productVariation): Response
    {
        return $this->render('product/listPicturesVariation.html.twig', [
            'productVariation' => $productVariation
        ]);
    }

    //listproductvariationsize
    #[Route('/variation/options/{id}', name: 'app_admin_product_size_variation_list', methods: ['GET'])]
    public function listProductVariationSize(ProductVariation $productVariation): Response
    {
        return $this->render('product/listProductVariationSize.html.twig', [
            'productVariation' => $productVariation
        ]);
    }



    //add picturevariation
    #[Route('/pictures/variation/{id}/new', name: 'app_admin_productvariation_picture_new', methods: ['GET', 'POST'])]
    public function newVariationPicture(ProductVariation $productVariation, Request $request, SluggerInterface $sluggerInterface, PictureRepository $pictureRepository, Uploader $uploader): Response
    {
        $picture = new Picture();

        $form = $this->createForm(PictureType::class, $picture);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $picture->setSlug($sluggerInterface->slug($picture->getName())->lower());
            $picture->setProductvariation($productVariation);
            $pictureUpload = $form->get('picture')->getData();
            if ($pictureUpload) {
                $path = $uploader->upload($pictureUpload, 'productvariation');
                $picture->setPath($path);
            }
            $pictureRepository->save($picture, true);

            return $this->redirectToRoute('app_admin_product_pictures_variation', ['id'=> $productVariation->getId()], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('product/newPicture.html.twig', [
            'form' => $form,
            'product' => $productVariation,
            'edit' => $picture->getId()
        ]);
    }


    //add productvariationsize
    #[Route('/variations/options/{id}/new', name: 'app_options_variation_new', methods: ['GET', 'POST'])]
    public function newOptions(ProductVariation $productVariation, Request $request, ProductvariationsizeRepository $productvariationsizeRepository): Response
    {
        $options = new Productvariationsize();
      
        $form = $this->createForm(ProductVariationsizeType::class, $options);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
          
            $productvariationsizeRepository->save($options, true);

            return $this->redirectToRoute('app_admin_product_size_variation_list', ['id'=> $productVariation->getId()], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('product/options.html.twig', [
            'form' => $form,
            'product' => $productVariation,
            'edit' => $options->getId()
        ]);
    }





    //add productvariation
    #[Route('/variations/{id}/new', name: 'app_product_variation_new', methods: ['GET', 'POST'])]
    public function newVariation(Product $product, Request $request, SluggerInterface $sluggerInterface, ProductVariationRepository $productvariationRepository): Response
    {
       
            $productVariation = new ProductVariation();
            $productVariation->setProduct($product);
    
    
       
        $form = $this->createForm(ProductVariationType::class, $productVariation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $productVariation -> setSlug($sluggerInterface->slug($productVariation->getSubtitle())->lower());
            $productvariationRepository->save($productVariation, true);

            return $this->redirectToRoute('app_product_variation_list', ['id'=> $product->getId()], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('product/newvariation.html.twig', [
            'form' => $form,
            'product' => $product,
            'edit' => $productVariation->getId()
        ]);
    }

   //add edit product
    #[Route('/new', name: 'app_product_new', methods: ['GET', 'POST'])]
    #[Route('/{id}/edit', name: 'app_product_edit', methods: ['GET', 'POST'])]
    public function new(Product $product=null,Request $request, ProductRepository $productRepository, SluggerInterface $sluggerInterface): Response
    {
        if($product==null)
        {
            $product = new Product();
            $product->addProductVariation(new ProductVariation());
        }
         
       
        $form = $this->createForm(ProductType::class, $product);
        $form->handleRequest($request);

        

        if ($form->isSubmitted() && $form->isValid()) {
                $product -> setSlug($sluggerInterface->slug($product->getTitle())->lower());
                $defaultVariation = $product->getProductVariations()[0];
                $defaultVariation->setSlug($sluggerInterface->slug($defaultVariation->getSubtitle())->lower());
                $product->setCreatedAt(new DateTimeImmutable('now'));
                $productRepository->save($product, true);
            
           
            return $this->redirectToRoute('app_product_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('product/new.html.twig', [
            'product' => $product,
            'form' => $form,
            'edit' => $product->getId()
            
        ]);
    }

    //show productdetail
    #[Route('/{id}', name: 'app_product_show', methods: ['GET'])]
    public function show(Product $product): Response
    {
        return $this->render('product/show.html.twig', [
            'product' => $product,
            
        ]);
    }

    
    //delete product
    #[Route('/{id}', name: 'app_product_delete', methods: ['POST'])]
    public function delete(Request $request, Product $product, ProductRepository $productRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$product->getId(), $request->request->get('_token'))) {
            $productRepository->remove($product, true);
        }

        return $this->redirectToRoute('app_product_index', [], Response::HTTP_SEE_OTHER);
    }
}
