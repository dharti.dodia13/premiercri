<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\Product;
use App\Entity\Category;
use App\Entity\ProductVariation;
use App\Entity\Productvariationsize;
use App\Repository\PictureRepository;

use App\Repository\ProductRepository;
use App\Repository\CategoryRepository;
use Symfony\Component\HttpFoundation\Request;
use App\Repository\ProductVariationRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\ProductvariationsizeRepository;
use Doctrine\ORM\Mapping\Id;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class HomeController extends AbstractController
{

    public function listCatMenu(AuthenticationUtils $authenticationUtils,CategoryRepository $categoryRepository): Response
    {
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('home/listCatMenu.html.twig', [
            'categories' => $categoryRepository->findBy(['parentid'=>null],['title'=>'ASC'])
        ]);
    }
    
    #[Route('/', name: 'app_home')]
    public function index(CategoryRepository $categoryRepository): Response
    {
        return $this->render('home/index.html.twig', [
            'categories' => $categoryRepository->findBy(['parentid'=>null],['title'=>'ASC'])
        ]);
    }

    #[Route('/category/{slug}', name: 'app_home_products_display')]
    public function productsDisplay(Category $category, CategoryRepository $categoryRepository, ProductRepository $productRepository): Response
    {
        //dump($productRepository->findDefault($category->getId()));
        $products = $category->getProducts();
        //dd ($category);
        //dd($categoryRepository->findBy(array(), array('id' => 'DESC')));

        // If categro have subcategory... display sub
        // AND if exists display product inside this category (category.products...)

            return $this->render('home/productsDisplay.html.twig', [
                'subcategories' => $categoryRepository->findBy(['parentid'=>$category->getId()],['title'=>'ASC']),
                'products'=> $products
               
            ]);

    }

    #[Route('/productdetails/{id}/{idVariation}', name: 'app_home_productsdetails')]
    public function productDetails(Product $product, $idVariation, ProductVariationRepository $productVariationRepository): Response
    {
       // dd($product);
        $currentVariation  = $product->getProductVariations()->filter(function(ProductVariation $productVariation) use ($idVariation) {
            return $productVariation->getId() == $idVariation;
        });

        // dump($currentVariation);
        return $this->render('home/productDetails.html.twig', [
            'product'=> $product,
            'variation'=>$currentVariation->first()
        ]);
    }



    #[Route('/account', name: 'app_home_account')]
    public function account(): Response
    {
        $userId =$this->getUser();
        return $this->render('home/account.html.twig', [
            'controller_name' => 'HomeController',
            'userId' => $userId,
        ]);
    }





/*
    #[Route('/category/{parentslug}/sub/{slug}/product', name: 'app_home_allproducts_display')]
    public function allprouctsDisplay(Category $category, ProductRepository $productRepository): Response
    {
        dd($productRepository->findAll());
        dd($productRepository->findBy(array('category.id'=>$category->getId(), 'category:parentid'=> $category->getParentid())));
            return $this->render('home/allproductsDisplay.html.twig', [
                'products' => $productRepository->findBy(['category.id'=>$category->getId()],['title'=>'ASC'])
            ]);
    }
*/


    #[Route('/panier', name: 'app_home_panier')]
    public function panier(SessionInterface $session): Response
    {
    
        return $this->render('home/cart.html.twig', [
            'panier' => $session->get("panier", [])
        ]);
    }


    // PANIER TESTING
    
    #[Route("panier/add/{id}", name:"addProduct")]
    public function add(ProductVariation $productVariation, ProductvariationsizeRepository $productvariationsizeRepository , SessionInterface $session, Request $request)
     {
        // $session->set("panier", []);  

        $sizeid = $request->request->get('size', null);

        $variationSize = null;
        $sizeTitle = null;
        
        if(!empty($sizeid)){
            $variationSize = $productvariationsizeRepository->find($sizeid);
            $sizeTitle =  $variationSize->getSizeid()->getSize();
        }
       
       

         //récupère le panier actuel, si le panier existe pas on le créer
        $panier = $session->get("panier", []);

        $id = $productVariation->getId();

        if(!empty($panier[$id])){
            $panier[$id]['quantity']++;
        }else{
            $panier[$id]['product'] = $productVariation;
            $panier[$id]['picture'] = $productVariation->getPictures()->first();
            $panier[$id]['size'] = $variationSize;
            $panier[$id]['sizeTitle'] = $sizeTitle;
            //dd($sizeTitle);

            /*$productVariation->getProductvariationsizes()->filter(function(Productvariationsize $productvariationsize) use ($sizeid){
                return $productvariationsize->getId() == $sizeid;
            });*/
    
            $panier[$id]['quantity'] = 1;
        }      
       //dd($panier);
        // sauvegarde dans la session
        $session->set("panier", $panier);   
       
        return $this->redirectToRoute("app_home_panier");
    }

   
    #[Route("panier/remove/{id}", name:"remove")]
    public function remove(ProductVariation $productVariation, SessionInterface $session)
    {
        // On récupère le panier actuel
        $panier = $session->get("panier", []);
        $id = $productVariation->getId();       
        if(!empty($panier[$id])){
            if($panier[$id]['quantity'] > 1){
                $panier[$id]['quantity']--;
            }else{
                unset($panier[$id]);
            }
        }
        // On sauvegarde dans la session
        $session->set("panier", $panier);
        return $this->redirectToRoute("app_home_panier");
    }

    
    #[Route("/delete/{id}", name:"delete")]
    public function delete(ProductVariation $productVariation, SessionInterface $session)
    {
        // On récupère le panier actuel
        $panier = $session->get("panier", []);
        $id = $productVariation->getId();
        if(!empty($panier[$id])){
            unset($panier[$id]);
        }
        // On sauvegarde dans la session
        $session->set("panier", $panier);
        return $this->redirectToRoute("app_home_panier");
    }
   
    #[Route("/delete", name:"delete_all")]
    public function deleteAll(SessionInterface $session)
    {
        $session->remove("panier");
        return $this->redirectToRoute("cart_index");
    }



    #[Route("/order", name:"order")]
    public function order(SessionInterface $session)
    {       
        $panier = $session->get("panier", []);
        $userId = $this->getUser();
        
              
        // dd($panier);
        return $this->render('order/order.html.twig', [
            'panier' => $panier,
            'userId' => $userId,
            
        ]);
    }


}


