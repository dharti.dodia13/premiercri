<?php

namespace App\Controller;

use App\Entity\ShoppingcartItem;
use App\Form\ShoppingcartItemType;
use App\Repository\ShoppingcartItemRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/shoppingcart/item')]
class ShoppingcartItemController extends AbstractController
{
    #[Route('/', name: 'app_shoppingcart_item_index', methods: ['GET'])]
    public function index(ShoppingcartItemRepository $shoppingcartItemRepository): Response
    {
        return $this->render('shoppingcart_item/index.html.twig', [
            'shoppingcart_items' => $shoppingcartItemRepository->findAll(),
        ]);
    }

   
    #[Route('/new', name: 'app_shoppingcart_item_new', methods: ['GET', 'POST'])]
    public function new(Request $request, ShoppingcartItemRepository $shoppingcartItemRepository): Response
    {
        $shoppingcartItem = new ShoppingcartItem();
        $form = $this->createForm(ShoppingcartItemType::class, $shoppingcartItem);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $shoppingcartItemRepository->save($shoppingcartItem, true);

            return $this->redirectToRoute('app_shoppingcart_item_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('shoppingcart_item/new.html.twig', [
            'shoppingcart_item' => $shoppingcartItem,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_shoppingcart_item_show', methods: ['GET'])]
    public function show(ShoppingcartItem $shoppingcartItem): Response
    {
        return $this->render('shoppingcart_item/show.html.twig', [
            'shoppingcart_item' => $shoppingcartItem,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_shoppingcart_item_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, ShoppingcartItem $shoppingcartItem, ShoppingcartItemRepository $shoppingcartItemRepository): Response
    {
        $form = $this->createForm(ShoppingcartItemType::class, $shoppingcartItem);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $shoppingcartItemRepository->save($shoppingcartItem, true);

            return $this->redirectToRoute('app_shoppingcart_item_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('shoppingcart_item/edit.html.twig', [
            'shoppingcart_item' => $shoppingcartItem,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_shoppingcart_item_delete', methods: ['POST'])]
    public function delete(Request $request, ShoppingcartItem $shoppingcartItem, ShoppingcartItemRepository $shoppingcartItemRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$shoppingcartItem->getId(), $request->request->get('_token'))) {
            $shoppingcartItemRepository->remove($shoppingcartItem, true);
        }

        return $this->redirectToRoute('app_shoppingcart_item_index', [], Response::HTTP_SEE_OTHER);
    }
}
