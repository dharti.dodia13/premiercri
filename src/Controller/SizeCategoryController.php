<?php

namespace App\Controller;

use App\Entity\SizeCategory;
use App\Form\SizeCategoryType;
use App\Repository\SizeCategoryRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/admin/sizecategory')]
class SizeCategoryController extends AbstractController
{
    #[Route('/', name: 'app_size_category_index', methods: ['GET'])]
    public function index(SizeCategoryRepository $sizeCategoryRepository): Response
    {
        return $this->render('size_category/index.html.twig', [
            'size_categories' => $sizeCategoryRepository->findAll(),
        ]);
    }

    #[Route('/{id}/edit', name: 'app_size_category_edit', methods: ['GET', 'POST'])]
    #[Route('/new', name: 'app_size_category_new', methods: ['GET', 'POST'])]
    public function new(SizeCategory $sizeCategory=null,Request $request, SizeCategoryRepository $sizeCategoryRepository): Response
    {
        if($sizeCategory == null)
            $sizeCategory = new SizeCategory();
        
        $form = $this->createForm(SizeCategoryType::class, $sizeCategory);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $sizeCategoryRepository->save($sizeCategory, true);

            return $this->redirectToRoute('app_size_category_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('size_category/new.html.twig', [
            'size_category' => $sizeCategory,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_size_category_show', methods: ['GET'])]
    public function show(SizeCategory $sizeCategory): Response
    {
        return $this->render('size_category/show.html.twig', [
            'size_category' => $sizeCategory,
        ]);
    }

   
    #[Route('/{id}', name: 'app_size_category_delete', methods: ['POST'])]
    public function delete(Request $request, SizeCategory $sizeCategory, SizeCategoryRepository $sizeCategoryRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$sizeCategory->getId(), $request->request->get('_token'))) {
            $sizeCategoryRepository->remove($sizeCategory, true);
        }

        return $this->redirectToRoute('app_size_category_index', [], Response::HTTP_SEE_OTHER);
    }
}
