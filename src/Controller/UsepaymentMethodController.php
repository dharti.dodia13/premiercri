<?php

namespace App\Controller;

use App\Entity\UsepaymentMethod;
use App\Form\UsepaymentMethodType;
use App\Repository\UsepaymentMethodRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/usepayment/method')]
class UsepaymentMethodController extends AbstractController
{
    #[Route('/', name: 'app_usepayment_method_index', methods: ['GET'])]
    public function index(UsepaymentMethodRepository $usepaymentMethodRepository): Response
    {
        return $this->render('usepayment_method/index.html.twig', [
            'usepayment_methods' => $usepaymentMethodRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_usepayment_method_new', methods: ['GET', 'POST'])]
    public function new(Request $request, UsepaymentMethodRepository $usepaymentMethodRepository): Response
    {
        $usepaymentMethod = new UsepaymentMethod();
        $form = $this->createForm(UsepaymentMethodType::class, $usepaymentMethod);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $usepaymentMethodRepository->save($usepaymentMethod, true);

            return $this->redirectToRoute('app_usepayment_method_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('usepayment_method/new.html.twig', [
            'usepayment_method' => $usepaymentMethod,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_usepayment_method_show', methods: ['GET'])]
    public function show(UsepaymentMethod $usepaymentMethod): Response
    {
        return $this->render('usepayment_method/show.html.twig', [
            'usepayment_method' => $usepaymentMethod,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_usepayment_method_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, UsepaymentMethod $usepaymentMethod, UsepaymentMethodRepository $usepaymentMethodRepository): Response
    {
        $form = $this->createForm(UsepaymentMethodType::class, $usepaymentMethod);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $usepaymentMethodRepository->save($usepaymentMethod, true);

            return $this->redirectToRoute('app_usepayment_method_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('usepayment_method/edit.html.twig', [
            'usepayment_method' => $usepaymentMethod,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_usepayment_method_delete', methods: ['POST'])]
    public function delete(Request $request, UsepaymentMethod $usepaymentMethod, UsepaymentMethodRepository $usepaymentMethodRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$usepaymentMethod->getId(), $request->request->get('_token'))) {
            $usepaymentMethodRepository->remove($usepaymentMethod, true);
        }

        return $this->redirectToRoute('app_usepayment_method_index', [], Response::HTTP_SEE_OTHER);
    }
}
