<?php

namespace App\Form;

use App\Entity\ShoppingCart;
use App\Entity\ShoppingcartItem;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ShoppingcartItemType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('quantity')
            ->add('shoppingcart',EntityType ::class,[
                'class' => ShoppingCart ::class,
                'choice_label' => 'id',
            ])
            ->add('sizevariation')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => ShoppingcartItem::class,
        ]);
    }
}
