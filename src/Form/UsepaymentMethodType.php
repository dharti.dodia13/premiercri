<?php

namespace App\Form;

use App\Entity\UsepaymentMethod;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UsepaymentMethodType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('provider')
            ->add('payment_type')
            ->add('account_number')
            ->add('expiry_date')
            ->add('orderdetails')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => UsepaymentMethod::class,
        ]);
    }
}
