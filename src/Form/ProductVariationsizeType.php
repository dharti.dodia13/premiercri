<?php

namespace App\Form;

use App\Entity\Size;
use App\Entity\Product;
use App\Entity\ProductVariation;
use App\Entity\Productvariationsize;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProductVariationsizeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('price')
            ->add('qty')
            ->add('sizeid', EntityType::class, [
                'class' => Size::class,
                'choice_label' => 'size',
            ])
            ->add('variationid', EntityType::class, [
                'class' => ProductVariation::class,
                'choice_label' => 'subtitle',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Productvariationsize::class,
        ]);
    }
}
