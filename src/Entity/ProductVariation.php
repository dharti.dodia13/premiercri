<?php

namespace App\Entity;

use App\Repository\ProductVariationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ProductVariationRepository::class)]
class ProductVariation
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'productVariations')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Product $product = null;


    #[ORM\OneToMany(mappedBy: 'productvariation', targetEntity: Picture::class)]
    private Collection $pictures;

    #[ORM\Column(length: 255)]
    private ?string $subtitle = null;

    #[ORM\Column(length: 255)]
    private ?string $discrition = null;

    #[ORM\Column]
    private ?float $price = null;

    #[ORM\Column(length: 255)]
    private ?string $slug = null;

    #[ORM\Column(nullable: true)]
    private ?int $qty = null;

    #[ORM\Column]
    private ?bool $defaultvariation = null;

    #[ORM\OneToMany(mappedBy: 'variationid', targetEntity: Productvariationsize::class)]
    private Collection $productvariationsizes;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $colour = null;

    

    public function __construct()
    {
        $this->pictures = new ArrayCollection();
        $this->productvariationsizes = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }


    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    
  

    /**
     * @return Collection<int, Picture>
     */
    public function getPictures(): Collection
    {
        return $this->pictures;
    }

    public function addPicture(Picture $picture): self
    {
        if (!$this->pictures->contains($picture)) {
            $this->pictures->add($picture);
            $picture->setProductvariation($this);
        }

        return $this;
    }

    public function removePicture(Picture $picture): self
    {
        if ($this->pictures->removeElement($picture)) {
            // set the owning side to null (unless already changed)
            if ($picture->getProductvariation() === $this) {
                $picture->setProductvariation(null);
            }
        }

        return $this;
    }

    public function getSubtitle(): ?string
    {
        return $this->subtitle;
    }

    public function setSubtitle(string $subtitle): self
    {
        $this->subtitle = $subtitle;

        return $this;
    }

    public function getDiscrition(): ?string
    {
        return $this->discrition;
    }

    public function setDiscrition(string $discrition): self
    {
        $this->discrition = $discrition;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getQty(): ?int
    {
        return $this->qty;
    }

    public function setQty(?int $qty): self
    {
        $this->qty = $qty;

        return $this;
    }

    public function isDefaultvariation(): ?bool
    {
        return $this->defaultvariation;
    }

    public function setDefaultvariation(bool $defaultvariation): self
    {
        $this->defaultvariation = $defaultvariation;

        return $this;
    }

    /**
     * @return Collection<int, Productvariationsize>
     */
    public function getProductvariationsizes(): Collection
    {
        return $this->productvariationsizes;
    }

    public function addProductvariationsize(Productvariationsize $productvariationsize): self
    {
        if (!$this->productvariationsizes->contains($productvariationsize)) {
            $this->productvariationsizes->add($productvariationsize);
            $productvariationsize->setVariationid($this);
        }

        return $this;
    }

    public function removeProductvariationsize(Productvariationsize $productvariationsize): self
    {
        if ($this->productvariationsizes->removeElement($productvariationsize)) {
            // set the owning side to null (unless already changed)
            if ($productvariationsize->getVariationid() === $this) {
                $productvariationsize->setVariationid(null);
            }
        }

        return $this;
    }

    public function getColour(): ?string
    {
        return $this->colour;
    }

    public function setColour(?string $colour): self
    {
        $this->colour = $colour;

        return $this;
    }

    

}
