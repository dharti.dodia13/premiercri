<?php

namespace App\Entity;

use App\Repository\OrdersRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: OrdersRepository::class)]
class Orders
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column]
    private ?\DateTimeImmutable $creates_At = null;

    #[ORM\Column]
    private ?\DateTimeImmutable $paid_At = null;

    #[ORM\Column]
    private ?\DateTimeImmutable $shipping_At = null;

    #[ORM\Column]
    private ?\DateTimeImmutable $delivered_At = null;

    #[ORM\Column]
    private ?float $shipping_cost = null;

    #[ORM\ManyToOne(inversedBy: 'orders')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Customer $customer = null;

    #[ORM\OneToMany(mappedBy: 'orders', targetEntity: OrderDetails::class)]
    private Collection $orderdetails;

    public function __construct()
    {
        $this->orderdetails = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreatesAt(): ?\DateTimeImmutable
    {
        return $this->creates_At;
    }

    public function setCreatesAt(\DateTimeImmutable $creates_At): self
    {
        $this->creates_At = $creates_At;

        return $this;
    }

    public function getPaidAt(): ?\DateTimeImmutable
    {
        return $this->paid_At;
    }

    public function setPaidAt(\DateTimeImmutable $paid_At): self
    {
        $this->paid_At = $paid_At;

        return $this;
    }

    public function getShippingAt(): ?\DateTimeImmutable
    {
        return $this->shipping_At;
    }

    public function setShippingAt(\DateTimeImmutable $shipping_At): self
    {
        $this->shipping_At = $shipping_At;

        return $this;
    }

    public function getDeliveredAt(): ?\DateTimeImmutable
    {
        return $this->delivered_At;
    }

    public function setDeliveredAt(\DateTimeImmutable $delivered_At): self
    {
        $this->delivered_At = $delivered_At;

        return $this;
    }

    public function getShippingCost(): ?float
    {
        return $this->shipping_cost;
    }

    public function setShippingCost(float $shipping_cost): self
    {
        $this->shipping_cost = $shipping_cost;

        return $this;
    }

    public function getCustomer(): ?Customer
    {
        return $this->customer;
    }

    public function setCustomer(?Customer $customer): self
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * @return Collection<int, OrderDetails>
     */
    public function getOrderdetails(): Collection
    {
        return $this->orderdetails;
    }

    public function addOrderdetail(OrderDetails $orderdetail): self
    {
        if (!$this->orderdetails->contains($orderdetail)) {
            $this->orderdetails->add($orderdetail);
            $orderdetail->setOrders($this);
        }

        return $this;
    }

    public function removeOrderdetail(OrderDetails $orderdetail): self
    {
        if ($this->orderdetails->removeElement($orderdetail)) {
            // set the owning side to null (unless already changed)
            if ($orderdetail->getOrders() === $this) {
                $orderdetail->setOrders(null);
            }
        }

        return $this;
    }
}
