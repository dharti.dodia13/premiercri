<?php

namespace App\Entity;

use App\Repository\SizeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: SizeRepository::class)]
class Size
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $size = null;



    #[ORM\OneToMany(mappedBy: 'sizeid', targetEntity: Productvariationsize::class)]
    private Collection $productvariationsizes;

    #[ORM\ManyToOne(inversedBy: 'sizes')]
    #[ORM\JoinColumn(nullable: false)]
    private ?SizeCategory $sizecategory = null;

    #[ORM\Column]
    private ?int $sizeorder = null;


    public function __construct()
    {
      
        $this->productvariationsizes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSize(): ?string
    {
        return $this->size;
    }

    public function setSize(string $size): self
    {
        $this->size = $size;

        return $this;
    }



    /**
     * @return Collection<int, Productvariationsize>
     */
    public function getProductvariationsizes(): Collection
    {
        return $this->productvariationsizes;
    }

    public function addProductvariationsize(Productvariationsize $productvariationsize): self
    {
        if (!$this->productvariationsizes->contains($productvariationsize)) {
            $this->productvariationsizes->add($productvariationsize);
            $productvariationsize->setSizeid($this);
        }

        return $this;
    }

    public function removeProductvariationsize(Productvariationsize $productvariationsize): self
    {
        if ($this->productvariationsizes->removeElement($productvariationsize)) {
            // set the owning side to null (unless already changed)
            if ($productvariationsize->getSizeid() === $this) {
                $productvariationsize->setSizeid(null);
            }
        }

        return $this;
    }

    public function getSizecategory(): ?SizeCategory
    {
        return $this->sizecategory;
    }

    public function setSizecategory(?SizeCategory $sizecategory): self
    {
        $this->sizecategory = $sizecategory;

        return $this;
    }

    public function getSizeorder(): ?int
    {
        return $this->sizeorder;
    }

    public function setSizeorder(int $sizeorder): self
    {
        $this->sizeorder = $sizeorder;

        return $this;
    }

    
}
