<?php

namespace App\Entity;

use App\Repository\ProductvariationsizeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ProductvariationsizeRepository::class)]
class Productvariationsize
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column]
    private ?float $price = null;

    #[ORM\Column(nullable: true)]
    private ?int $qty = null;

    #[ORM\ManyToOne(inversedBy: 'productvariationsizes')]
    #[ORM\JoinColumn(nullable: false)]
    private ?ProductVariation $variationid = null;

    #[ORM\ManyToOne(inversedBy: 'productvariationsizes')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Size $sizeid = null;

    #[ORM\OneToMany(mappedBy: 'productvariationsize', targetEntity: Picture::class)]
    private Collection $pictures;

    public function __construct()
    {
        $this->pictures = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getQty(): ?int
    {
        return $this->qty;
    }

    public function setQty(?int $qty): self
    {
        $this->qty = $qty;

        return $this;
    }

    public function getVariationid(): ?ProductVariation
    {
        return $this->variationid;
    }

    public function setVariationid(?ProductVariation $variationid): self
    {
        $this->variationid = $variationid;

        return $this;
    }

    public function getSizeid(): ?Size
    {
        return $this->sizeid;
    }

    public function setSizeid(?Size $sizeid): self
    {
        $this->sizeid = $sizeid;

        return $this;
    }

    /**
     * @return Collection<int, Picture>
     */
    public function getPictures(): Collection
    {
        return $this->pictures;
    }

    public function addPicture(Picture $picture): self
    {
        if (!$this->pictures->contains($picture)) {
            $this->pictures->add($picture);
            $picture->setProductvariationsize($this);
        }

        return $this;
    }

    public function removePicture(Picture $picture): self
    {
        if ($this->pictures->removeElement($picture)) {
            // set the owning side to null (unless already changed)
            if ($picture->getProductvariationsize() === $this) {
                $picture->setProductvariationsize(null);
            }
        }

        return $this;
    }
}
