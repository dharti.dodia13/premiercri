<?php

namespace App\Entity;

use App\Repository\OrderDetailsRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: OrderDetailsRepository::class)]
class OrderDetails
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column]
    private ?int $quantity = null;

    #[ORM\Column]
    private ?float $price = null;

    #[ORM\ManyToOne(inversedBy: 'orderdetails')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Orders $orders = null;




    #[ORM\OneToMany(mappedBy: 'orderdetails', targetEntity: UsepaymentMethod::class)]
    private Collection $usepaymentMethods;

    public function __construct()
    {
        $this->usepaymentMethods = new ArrayCollection();
        $this->usepaymentMethods = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getOrders(): ?Orders
    {
        return $this->orders;
    }

    public function setOrders(?Orders $orders): self
    {
        $this->orders = $orders;

        return $this;
    }



    /**
     * @return Collection<int, UsepaymentMethod>
     */
    public function getUsepaymentMethods(): Collection
    {
        return $this->usepaymentMethods;
    }

    public function addUsepaymentMethod(UsepaymentMethod $usepaymentMethod): self
    {
        if (!$this->usepaymentMethods->contains($usepaymentMethod)) {
            $this->usepaymentMethods->add($usepaymentMethod);
            $usepaymentMethod->setOrderdetails($this);
        }

        return $this;
    }

    public function removeUsepaymentMethod(UsepaymentMethod $usepaymentMethod): self
    {
        if ($this->usepaymentMethods->removeElement($usepaymentMethod)) {
            // set the owning side to null (unless already changed)
            if ($usepaymentMethod->getOrderdetails() === $this) {
                $usepaymentMethod->setOrderdetails(null);
            }
        }

        return $this;
    }
}
