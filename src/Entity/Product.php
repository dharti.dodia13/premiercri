<?php

namespace App\Entity;

use App\Entity\Picture;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\ProductRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

#[ORM\Entity(repositoryClass: ProductRepository::class)]
class Product
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $title = null;

    #[ORM\Column(nullable: true)]
    private ?int $price = null;

    #[ORM\Column(length: 255)]
    private ?string $slug = null;

    #[ORM\Column]
    private ?\DateTimeImmutable $created_At = null;


    #[ORM\OneToMany(mappedBy: 'product', targetEntity: ProductVariation::class, cascade: ['persist'])]
    private Collection $productVariations;


    #[ORM\Column(nullable: true)]
    private ?\DateTimeImmutable $modified_At = null;

    #[ORM\ManyToOne(inversedBy: 'products')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Category $category = null;


    #[ORM\Column(nullable: true)]
    private ?int $totalqty = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $defaultdiscription = null;

     
    public function __construct()
    {
        $this->productVariations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getPrice(): ?int
    {
        return $this->price;
    }

    public function setPrice(int $price): self
    {
        $this->price = $price;

        return $this;
    }

   

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->created_At;
    }

    public function setCreatedAt(\DateTimeImmutable $created_At): self
    {
        $this->created_At = $created_At;

        return $this;
    }

   

    /**
     * @return Collection<int, ProductVariation>
     */
    public function getProductVariations(): Collection
    {
        return $this->productVariations;
    }

     /**
     * @return ProductVariation ProductVariation
     */
    public function getProductDefaultVariations(): Collection
    {
        return $this->getProductVariations()->filter(function(ProductVariation $productVariation) {
            return $productVariation->isDefaultvariation() == true;
        });
    }

    public function addProductVariation(ProductVariation $productVariation): self
    {
        if (!$this->productVariations->contains($productVariation)) {
            $this->productVariations->add($productVariation);
            $productVariation->setProduct($this);
        }

        return $this;
    }

    public function removeProductVariation(ProductVariation $productVariation): self
    {
        if ($this->productVariations->removeElement($productVariation)) {
            // set the owning side to null (unless already changed)
            if ($productVariation->getProduct() === $this) {
                $productVariation->setProduct(null);
            }
        }

        return $this;
    }

    

    public function getModifiedAt(): ?\DateTimeImmutable
    {
        return $this->modified_At;
    }

    public function setModifiedAt(?\DateTimeImmutable $modified_At): self
    {
        $this->modified_At = $modified_At;

        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): self
    {
        $this->category = $category;

        return $this;
    }


    public function getTotalqty(): ?int
    {
        return $this->totalqty;
    }

    public function setTotalqty(int $totalqty): self
    {
        $this->totalqty = $totalqty;

        return $this;
    }

    public function getDefaultdiscription(): ?string
    {
        return $this->defaultdiscription;
    }

    public function setDefaultdiscription(?string $defaultdiscription): self
    {
        $this->defaultdiscription = $defaultdiscription;

        return $this;
    }

     

}
